# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Max Lin <max7442@gmail.com>, 2011.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: dos@dosowisko.net\n"
"POT-Creation-Date: 2019-04-10 21:34+0200\n"
"PO-Revision-Date: 2011-10-28 12:53+0800\n"
"Last-Translator: Max Lin <max7442@gmail.com>\n"
"Language-Team: Chinese Traditional <kde-i18n-doc@kde.org>\n"
"Language: zh_TW\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 1.2\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: src/kamerka.qml:248
msgid "Take a photo"
msgstr "拍張照片"

#: src/kamerka.qml:263 src/settingsdialog.cpp:156
msgid "Burst mode"
msgstr ""

#: src/kamerka.qml:286 src/settingsdialog.cpp:144
msgid "Self-timer"
msgstr "自拍"

#: src/kamerka.qml:302
msgid "Hide effects"
msgstr ""

#: src/kamerka.qml:302
msgid "Show effects"
msgstr ""

#: src/kamerka.qml:318
msgid "Open directory"
msgstr "開啟目錄"

#: src/kamerka.qml:333
msgid "Configure"
msgstr "配置"

#: src/kamerka.qml:537
msgid "No Effect"
msgstr ""

#: src/kamerka.qml:553
msgid "Grey"
msgstr ""

#: src/kamerka.qml:569
msgid "Invert"
msgstr ""

#: src/kamerka.qml:585
msgid "Equalize"
msgstr ""

#: src/kamerka.qml:601
msgid "Smurf"
msgstr ""

#: src/kamerka.qml:616
msgid "Implode"
msgstr ""

#: src/kamerka.qml:631
msgid "Explode"
msgstr ""

#: src/kamerka.qml:646
msgid "Charcoal"
msgstr ""

#: src/kamerka.qml:661
msgid "Edge"
msgstr ""

#: src/kamerka.qml:676
msgid "Emboss"
msgstr ""

#: src/kamerka.qml:691
msgid "Swirl"
msgstr ""

#: src/kamerka.qml:706
msgid "Oil Paint"
msgstr ""

#: src/kamerka.qml:721
msgid "Wave"
msgstr ""

#: src/main.cpp:37
msgid "Kamerka"
msgstr "Kamerka"

#: src/main.cpp:38
msgid "Simple photo taking application with fancy animated interface"
msgstr "簡易的拍照應用程式與花俏的動畫介面"

#: src/main.cpp:39
#, fuzzy
msgid "Copyright (C) 2011-2014 Sebastian Krzyszkowiak"
msgstr "Copyright (c) 2011 Sebastian Krzyszkowiak"

#: src/mainwindow.cpp:46
msgid "i18n() takes at least one argument"
msgstr "i18n() 至少需要一個參數"

#: src/mainwindow.cpp:64
msgid "Could not load QML interface!"
msgstr "不能載入 QML 介面!"

#: src/mainwindow.cpp:64 src/mainwindow.cpp:115 src/mainwindow.cpp:138
msgid "Error"
msgstr "錯誤"

#: src/mainwindow.cpp:111 src/mainwindow.cpp:115 src/mainwindow.cpp:138
msgid "Could not connect to V4L device!"
msgstr "不能連結到 V4L 設備!"

#: src/mainwindow.cpp:157
msgid ""
"Requested resolution (%1x%2) was not available. Driver used %3x%4 instead.\n"
"Please check your configuration."
msgstr ""
"請求的解析度 (%1x%2) 不可用。驅動程式使用 %3x%4 代替。\n"
"請檢查您的配置。"

#: src/settingsdialog.cpp:43
msgid "Selected path does not exists. Do you want to create it?"
msgstr "選擇的路徑不存在。您想建立它嗎？"

#: src/settingsdialog.cpp:60
msgid "Device node:"
msgstr "設備節點:"

#: src/settingsdialog.cpp:67
msgid "x"
msgstr "x"

#: src/settingsdialog.cpp:72
msgid "px"
msgstr "px"

#: src/settingsdialog.cpp:73
msgid "Resolution:"
msgstr "解析度:"

#: src/settingsdialog.cpp:78
msgid "Disabled"
msgstr "關閉"

#: src/settingsdialog.cpp:79
msgid " fps"
msgstr "fps"

#: src/settingsdialog.cpp:81
msgid "Framerate limit:"
msgstr "幀率限制:"

#: src/settingsdialog.cpp:83
msgid "Lock aspect ratio"
msgstr ""

#: src/settingsdialog.cpp:86
msgid "Enhance contrast"
msgstr ""

#: src/settingsdialog.cpp:89
msgid "Mirror output"
msgstr ""

#: src/settingsdialog.cpp:92
msgid "Flip output"
msgstr ""

#: src/settingsdialog.cpp:97
#, fuzzy
msgid "Image Settings"
msgstr "攝影機設定"

#: src/settingsdialog.cpp:107
msgid "Brightness:"
msgstr ""

#: src/settingsdialog.cpp:116
msgid "Contrast:"
msgstr ""

#: src/settingsdialog.cpp:125
msgid "Saturation:"
msgstr ""

#: src/settingsdialog.cpp:134
msgid "Hue:"
msgstr ""

#: src/settingsdialog.cpp:138
msgid "Camera"
msgstr "攝影機"

#: src/settingsdialog.cpp:138
msgid "Camera settings"
msgstr "攝影機設定"

#: src/settingsdialog.cpp:149 src/settingsdialog.cpp:167
msgid " seconds"
msgstr ""

#: src/settingsdialog.cpp:152
#, fuzzy
msgid "Self-timer timeout:"
msgstr "自拍"

#: src/settingsdialog.cpp:163
msgid "Number of photos:"
msgstr ""

#: src/settingsdialog.cpp:170
msgid "Delay between photos:"
msgstr ""

#: src/settingsdialog.cpp:174
msgid "Capture"
msgstr ""

#: src/settingsdialog.cpp:182
msgid "Use default pictures directory"
msgstr "使用預設的圖片目錄"

#: src/settingsdialog.cpp:189
msgid "Use subdirectory:"
msgstr "使用子目錄:"

#: src/settingsdialog.cpp:202
msgid "Photo directory:"
msgstr "照片目錄:"

#: src/settingsdialog.cpp:204
msgid "Storage"
msgstr "儲存"

#: src/settingsdialog.cpp:204
msgid "Photo storage"
msgstr "照片儲存"

#: src/settingsdialog.cpp:210
msgid "Play sound on taking photo"
msgstr "拍照時播放音效"

#: src/settingsdialog.cpp:213
msgid "Play timer sounds"
msgstr "播放計時音效"

#: src/settingsdialog.cpp:216
msgid "Show notification on taking photo"
msgstr "拍照時顯示通知"

#: src/settingsdialog.cpp:220
msgid "Behaviour"
msgstr "行為"

#: src/videowidget.cpp:100
msgid "Starting up webcam..."
msgstr "啟動網路攝影機..."

#: src/videowidget.cpp:170
msgid "Photo has been stored in file %1"
msgstr "照片已儲存在檔案 %1"

#: src/videowidget.cpp:175
msgid "Open"
msgstr ""

#: src/videowidget.cpp:175
msgid "Show in directory"
msgstr "在目錄中顯示"

#: tools/rc.cpp:1 rc.cpp:1
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Max Lin"

#: tools/rc.cpp:2 rc.cpp:2
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "max7442@gmail.com"

#~ msgid "Open in GIMP"
#~ msgstr "在GIMP中開啟"

#~ msgid "Open in Inkscape"
#~ msgstr "在Inkscape中開啟"

#~ msgid "Less"
#~ msgstr "顯示較少"

#~ msgid "More"
#~ msgstr "顯示更多"
